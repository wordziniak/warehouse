# Zadanie Praktyczne - Java

## Rozwiąznie

### Program można uruchomić łatwo uruchomić:

./mvnw spring-boot:run


### SwaggerUI dostępny jest standardowo pod adresem:
http://localhost:8080/swagger-ui.html


### Sama aplikacja wystawia api pod adresem:
http://localhost:8080/api/v1/

np:

http://localhost:8080/api/v1/product

http://localhost:8080/api/v1/stock

http://localhost:8080/api/v1/warehouse

## Zagadnienie do rozwiązania

Jesteś zobowiązany dostarczyć oprogramowanie wspomagające pracę hurtowni farmaceutycznej.

Zadanie polega na stworzeniu aplikacji (wystarczy API REST + SwaggerUI), która udostępni następujące usługi:

- Produkty
  - dodawanie produktu
  - zwrócenie listy
  - zwrócenie po nazwie produktu lub identyfikatorze

- Magazyn 
  - modyfikacja stanów magazynowych produktu
  - zwrócenie listy stanów
  - zwrócenie stanu magazynowego dla zadanego identyfikatora produktu

Należy pamiętać, że aplikacja powinna być odpowiednio zabezpieczona i dostosowana do obsługi błędów.

## Implementacja

- Rozwiązanie powinno być zaimplementowane w języku Java (wersja min 8)
- Kod powinien przedstawiać dobre praktyki kodowania obiektowego.
- Dozwolone jest użycie bibliotek pomocniczych lub baz danych (relacyjne/nierelacyjne/baza w pamięci). W przypadku bazy danych należy zwrócić uwagę na łatwość uruchomienia 
  projektu.
- Ocenie podlegać będzie jakość kodu oraz funkcjonalność i jakość samej aplikacji.
- Możliwe wykorzystanie systemów ułatwiających budowania aplikacji (Maven lub Gradle) oraz wykorzystania SpringBoot.

## Wysyłka rozwiązanego zadania

Preferowany sposób dostarczenia rozwiązania to link do repozytorium GIT. Może to być GitHub, GitLab lub hostowany git.

Najważniejsze, by można było zaciągnąć źródła wywołując polecenie git clone LINK.

Jeśli nie chcesz skorzystać z tej opcji, prosimy o link do pliku ZIP. Plik powinien być łatwy w identyfikacji i zawierać imię i nazwisko. 



