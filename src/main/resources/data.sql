DROP TABLE IF EXISTS STOCK;
DROP TABLE IF EXISTS WAREHOUSE;
DROP TABLE IF EXISTS PRODUCT;


CREATE TABLE WAREHOUSE (
                              ID INT AUTO_INCREMENT  PRIMARY KEY,
                              NAME VARCHAR(250) NOT NULL
);

CREATE TABLE PRODUCT (
                         ID INT AUTO_INCREMENT PRIMARY KEY,
                         EXTERNAL_ID VARCHAR(250) NOT NULL,
                         NAME VARCHAR(250) NOT NULL,
                         UNIQUE(EXTERNAL_ID)
);

CREATE TABLE STOCK (
                       ID INT AUTO_INCREMENT PRIMARY KEY,
                       PRODUCT_ID INT NOT NULL,
                       WAREHOUSE_ID INT NOT NULL,
                       QUANTITY INT NOT NULL,
                       FOREIGN KEY (PRODUCT_ID) REFERENCES PRODUCT(ID),
                       FOREIGN KEY (WAREHOUSE_ID) REFERENCES WAREHOUSE(ID),
                       UNIQUE (PRODUCT_ID, WAREHOUSE_ID)
);


INSERT INTO WAREHOUSE (NAME) VALUES
('warehouse1'),
('warehouse2'),
('warehouse3');



/* PRODUCT */


INSERT INTO PRODUCT (EXTERNAL_ID, NAME) VALUES
('product1_1', 'product1'),
('product1_2', 'product1'),
('product2_1', 'product2'),
('product2_2', 'product2'),
('product3_1', 'product3'),
('product4_1', 'product4');



INSERT INTO STOCK (PRODUCT_ID, WAREHOUSE_ID, QUANTITY) VALUES
    (1, 1, 1),
    (1, 2, 2),
    (1, 3, 3),
    (2, 1, 4),
    (2, 2, 5),
    (2, 3, 6),
    (3, 1, 7),
    (3, 2, 7),
    (4, 3, 8),
    (4, 1, 9),
    (5, 1, 10);

