package com.pharmacy.warehouse.service.repository;

import java.util.Collection;

import com.pharmacy.warehouse.service.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {

    Collection<Stock> findAllByWarehouse_id(Long id);

    Collection<Stock> findAllByProduct_id(Long id);

    Stock findByWarehouse_Id_AndProduct_Id(Long warehouseId, Long productId);
}
