package com.pharmacy.warehouse.service.repository;


import java.util.Collection;

import com.pharmacy.warehouse.service.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findByExternalId(String externalId);

    Collection<Product> findByName(String name);
}
