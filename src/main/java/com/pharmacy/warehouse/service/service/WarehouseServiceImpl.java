package com.pharmacy.warehouse.service.service;

import java.util.Collection;
import java.util.Optional;

import com.pharmacy.warehouse.service.model.Product;
import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.model.Warehouse;
import com.pharmacy.warehouse.service.repository.ProductRepository;
import com.pharmacy.warehouse.service.repository.StockRepository;
import com.pharmacy.warehouse.service.repository.WarehouseRepository;
import org.springframework.stereotype.Service;

@Service
public class WarehouseServiceImpl implements WarehouseService {

    private final WarehouseRepository warehouseRepository;
    private final StockRepository stockRepository;
    private final ProductRepository productRepository;

    public WarehouseServiceImpl(WarehouseRepository warehouseRepository, StockRepository stockRepository, ProductRepository productRepository) {
        this.warehouseRepository = warehouseRepository;
        this.stockRepository = stockRepository;
        this.productRepository = productRepository;
    }

    @Override
    public Collection<Warehouse> findAllWarehouses() {
        return warehouseRepository.findAll();
    }

    @Override
    public Collection<Stock> findWarehouseStock(Long warehouseId) {
        return stockRepository.findAllByWarehouse_id(warehouseId);
    }

    @Override
    public Optional<Stock> findProductInWarehouse(Long warehouseId, Long productId) {
        return Optional.ofNullable(stockRepository.findByWarehouse_Id_AndProduct_Id(warehouseId, productId));
    }

    @Override
    public Optional<Stock> changeProductQuantity(Long warehouseId, Long productId, Long quantity) {
        Stock productStock = stockRepository.findByWarehouse_Id_AndProduct_Id(warehouseId, productId);
        if(productStock != null) {
            if (quantity <= 0) {
                stockRepository.delete(productStock);
                return Optional.empty();
            }
            productStock.setQuantity(quantity);
            stockRepository.save(productStock);
        }
        else {
            Optional<Product> product = productRepository.findById(productId);
            Optional<Warehouse> warehouse = warehouseRepository.findById(warehouseId);
            if(product.isPresent() && warehouse.isPresent()) {
                productStock = createNewStock(warehouse.get(), product.get(), quantity);
            }
        }
        return Optional.ofNullable(productStock);
    }

    private Stock createNewStock(Warehouse warehouse, Product product, Long quantity){
        Stock newStock = new Stock();
        newStock.setProduct(product);
        newStock.setWarehouse(warehouse);
        newStock.setQuantity(quantity);
        return stockRepository.saveAndFlush(newStock);
    }

}