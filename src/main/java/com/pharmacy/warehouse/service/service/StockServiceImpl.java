package com.pharmacy.warehouse.service.service;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.repository.StockRepository;
import org.springframework.stereotype.Service;

@Service
public class StockServiceImpl implements StockService {

    private final StockRepository stockRepository;

    public StockServiceImpl(StockRepository stockRepository) {
        this.stockRepository = stockRepository;
    }

    @Override
    public Collection<Stock> findAllStocks() {
        return stockRepository.findAll();
    }

    @Override
    public Optional<Stock> changeProductStock(Stock stock) {
        AtomicReference<Stock> newStock = new AtomicReference<>();
        stockRepository.findById(stock.getId()).ifPresentOrElse(stockToUpdate -> {
                                                                    newStock.set(updateStock(stockToUpdate, stock));
                                                                }
                , () -> newStock.set(createNewStock(stock)));
        return Optional.ofNullable(newStock.get());

    }

    private Stock createNewStock(Stock stock) {
        return stockRepository.save(stock);
    }

    private Stock updateStock(Stock stockToUpdate, Stock newStock) {
        if (newStock.getQuantity() <= 0) {
            stockRepository.delete(stockToUpdate);
        } else {
            stockToUpdate.setQuantity(newStock.getQuantity());
            stockRepository.save(stockToUpdate);
        }
        return stockToUpdate;
    }
}
