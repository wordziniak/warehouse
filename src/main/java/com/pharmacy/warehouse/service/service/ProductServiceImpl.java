package com.pharmacy.warehouse.service.service;

import java.util.Collection;
import java.util.Optional;

import com.pharmacy.warehouse.service.model.Product;
import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.repository.ProductRepository;
import com.pharmacy.warehouse.service.repository.StockRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final StockRepository stockRepository;

    public ProductServiceImpl(ProductRepository productRepository, StockRepository stockRepository) {
        this.productRepository = productRepository;
        this.stockRepository = stockRepository;
    }

    @Override
    public Collection<Product> findAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Collection<Stock> findStockProduct(Long id) {
        return stockRepository.findAllByProduct_id(id);
    }

    @Override
    public Optional<Product> findProductByExternalId(String externalId) {
        return Optional.ofNullable(productRepository.findByExternalId(externalId));
    }

    @Override
    public Collection<Product> findProductByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    public Optional<Product> findProduct(Long id) {
        return productRepository.findById(id);
    }

    @Override
    public Optional<Product> addProduct(Product product) {
        if(productRepository.findById(product.getId()).isEmpty()){
            if (productRepository.findByExternalId(product.getExternalId()) == null) {
                return Optional.of(productRepository.save(product));
            }
        }
        return Optional.empty();
    }
}
