package com.pharmacy.warehouse.service.service;

import java.util.Collection;
import java.util.Optional;

import com.pharmacy.warehouse.service.model.Product;
import com.pharmacy.warehouse.service.model.Stock;

public interface ProductService {

    Collection<Product> findAllProducts();

    Collection<Stock> findStockProduct(Long id);

    Optional<Product> findProductByExternalId(String id);

    Collection<Product> findProductByName(String id);

    Optional<Product> findProduct(Long id);

    Optional<Product> addProduct(Product product);
}
