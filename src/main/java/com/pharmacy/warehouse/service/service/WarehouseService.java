package com.pharmacy.warehouse.service.service;

import java.util.Collection;
import java.util.Optional;

import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.model.Warehouse;

public interface WarehouseService {

    Collection<Warehouse> findAllWarehouses();

    Collection<Stock> findWarehouseStock(Long warehouseId);

    Optional<Stock> findProductInWarehouse(Long warehouseId, Long productId);

    Optional<Stock> changeProductQuantity(Long warehouseId, Long productId, Long quantity);
}
