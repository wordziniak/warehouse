package com.pharmacy.warehouse.service.service;

import java.util.Collection;
import java.util.Optional;

import com.pharmacy.warehouse.service.model.Stock;

public interface StockService {

    Collection<Stock> findAllStocks();

    Optional<Stock> changeProductStock(Stock stock);

}
