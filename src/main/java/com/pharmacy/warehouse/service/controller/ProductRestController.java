package com.pharmacy.warehouse.service.controller;

import java.util.Collection;

import com.pharmacy.warehouse.service.model.Product;
import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.pharmacy.warehouse.service.controller.ApiV1.PRODUCT;

@RestController
@RequestMapping(value = PRODUCT, produces = "application/json")
public class ProductRestController {

    private final ProductService productService;

    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    Collection<Product> getAllProduct(){
        return productService.findAllProducts();
    }

    @PostMapping
    ResponseEntity<Product> createNewProduct(@RequestBody Product product){
        return productService.addProduct(product).map(ResponseEntity::ok).orElse(ResponseEntity.status(HttpStatus.CONFLICT).build());
    }

    @GetMapping("/{productId}")
    ResponseEntity<Product> getProduct(@PathVariable Long productId){
        return ResponseEntity.of(productService.findProduct(productId));
    }

    @GetMapping("/{productId}/stock")
    Collection<Stock> getStockProduct(@PathVariable Long productId){
        return productService.findStockProduct(productId);
    }

    @GetMapping("/byExternalId")
    ResponseEntity<Product> getProductByExternalId(@RequestParam String externalId){
        return ResponseEntity.of(productService.findProductByExternalId(externalId));
    }

    @GetMapping("/byName")
    Collection<Product> getProductByName(@RequestParam String name){
        return productService.findProductByName(name);
    }

}
