package com.pharmacy.warehouse.service.controller;

import java.util.Collection;

import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.model.Warehouse;
import com.pharmacy.warehouse.service.service.WarehouseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.pharmacy.warehouse.service.controller.ApiV1.WAREHOUSE;

@RestController
@RequestMapping(value = WAREHOUSE, produces = "application/json")
public class WarehouseRestController {

    private final WarehouseService warehouseService;

    public WarehouseRestController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @GetMapping
    Collection<Warehouse> getAllWarehouses(){
        return warehouseService.findAllWarehouses();
    }

    @GetMapping("/{warehouseId}/stock")
    Collection<Stock> getWarehouseStock(@PathVariable Long warehouseId){
        return warehouseService.findWarehouseStock(warehouseId);
    }

    @GetMapping("/{warehouseId}/stock/{productId}")
    ResponseEntity<Stock> getStockProductFromWarehouse(@PathVariable Long warehouseId, @PathVariable Long productId){
        return ResponseEntity.of(warehouseService.findProductInWarehouse(warehouseId, productId));
    }

    @PutMapping("/{warehouseId}/stock/{productId}")
    ResponseEntity<Stock> changeQuantityStockProduct(@PathVariable Long warehouseId, @PathVariable Long productId, @RequestParam Long quantity){
        return ResponseEntity.of(warehouseService.changeProductQuantity(warehouseId, productId, quantity));
    }
}
