package com.pharmacy.warehouse.service.controller;

import java.util.Collection;

import com.pharmacy.warehouse.service.model.Product;
import com.pharmacy.warehouse.service.model.Stock;
import com.pharmacy.warehouse.service.model.Warehouse;
import com.pharmacy.warehouse.service.service.StockService;
import com.pharmacy.warehouse.service.service.WarehouseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.pharmacy.warehouse.service.controller.ApiV1.STOCK;
import static com.pharmacy.warehouse.service.controller.ApiV1.WAREHOUSE;

@RestController
@RequestMapping(value = STOCK, produces = "application/json")
public class StockRestController {

    private final StockService stockService;

    public StockRestController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping
    Collection<Stock> getAllStock(){
        return stockService.findAllStocks();
    }

    @PostMapping
    ResponseEntity<Stock> createNewProduct(@RequestBody Stock stock){
        return stockService.changeProductStock(stock).map(ResponseEntity::ok).orElse(ResponseEntity.status(HttpStatus.CONFLICT).build());
    }
}
