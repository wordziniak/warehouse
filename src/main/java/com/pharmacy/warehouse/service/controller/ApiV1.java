package com.pharmacy.warehouse.service.controller;

public class ApiV1 {

    private ApiV1(){}

    public static final String BASE = "/api/v1";
    public static final String WAREHOUSE = BASE + "/warehouse";
    public static final String PRODUCT = BASE + "/product";
    public static final String STOCK = BASE + "/stock";


}
