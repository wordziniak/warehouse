package com.pharmacy.warehouse.service.model;

import javax.persistence.*;

import java.util.Set;

import org.springframework.data.annotation.PersistenceConstructor;

@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String externalId;

    private String name;

    @PersistenceConstructor
    public Product() {}

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

//    public Stock getStock() {
//        return stock;
//    }
//
//    public void setStock(Stock stock) {
//        this.stock = stock;
//    }

    //    public Set getStocks() {
//        return stocks;
//    }
//
//    public void setStocks(Set stocks) {
//        this.stocks = stocks;
//    }
}
