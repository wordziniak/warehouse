package com.pharmacy.warehouse.service.model;

import javax.persistence.*;

import java.util.Set;

import org.springframework.data.annotation.PersistenceConstructor;

@Entity
@Table(name = "WAREHOUSE")
public class Warehouse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @PersistenceConstructor
    public Warehouse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
//
//    public Set<Stock> getStocks() {
//        return stocks;
//    }
//
//    public void setStocks(Set<Stock> stocks) {
//        this.stocks = stocks;
//    }
}
